import Layout from "../components/Layout";
import TutorialTest from "../components/TutorialTest";

export default function Test() {
    return (
        <Layout>
            <TutorialTest/>
        </Layout>
    );
}