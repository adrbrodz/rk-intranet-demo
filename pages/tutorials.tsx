import Layout from "../components/Layout";
import TutorialsPanel from "../components/TutorialsPanel";

export default function Tutorial() {
    return (
        <Layout>
            <TutorialsPanel/>
        </Layout>
    );
}