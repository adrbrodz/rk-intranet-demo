import AddNewEntry from "../components/AddNewEntry";
import Layout from "../components/Layout";
import React, { useState, useEffect } from 'react';

export default function MachineNotes() {
    return (
        <Layout>
            <AddNewEntry />
        </Layout>
    );
}