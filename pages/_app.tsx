import { AppProps } from 'next/app'
import '../styles/index.css'
import { CookiesProvider } from "react-cookie"
import {useState} from "react";
import LoginForm from "../components/LoginForm";
import Layout from "../components/Layout";

export default function MyApp({ Component, pageProps }: AppProps, data) {
    const [userLoggedIn, setUserLoggedIn] = useState(false);
    if (userLoggedIn === false) {
        return (
            <Layout>
                <LoginForm setUserLoggedIn={setUserLoggedIn}/>
            </Layout>
        )
    } else {
        return (
            <CookiesProvider>
                <Component {...pageProps} />
            </CookiesProvider>
        )
    }

}