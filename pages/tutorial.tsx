import Layout from "../components/Layout";
import TutorialTemplate from "../components/TutorialTemplate";

export default function Tutorial() {
    return (
        <Layout>
            <TutorialTemplate/>
        </Layout>
    );
}