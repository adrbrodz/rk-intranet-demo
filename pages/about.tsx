import cookie from "cookie";

function parseCookies(req) {

    return cookie.parse(req ? req.headers.cookie || "" : document.cookie)
}
import { useRouter } from "next/router";
import {useEffect} from "react";

export default function AboutPage({data}) {
    const router = useRouter();
    useEffect(() => {
        if (Object.keys(data).length === 0 ) {
            router.push("/login");
        }
    }, [data]);
    return (
        <>
            <div>
                <h1>Homepage </h1>
                <p>Data from cookie:</p>
            </div>
        </>
    )
}


AboutPage.getInitialProps = async ({ req, res }) => {
    const data = parseCookies(req)

    if (res) {
        if (Object.keys(data).length === 0 && data.constructor === Object) {
            res.writeHead(301, { Location: "/" })
            res.end()
        }
    }

    return {
        data: data && data,
    }
}