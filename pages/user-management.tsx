import React, { useState, useEffect } from 'react';
import Layout from "../components/Layout";

export default function UserManagement() {
    async function testRegister(credentials: any) {
        return fetch('http://localhost:3001/users/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        })

    }

    // States for registration
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');

    // States for checking the errors
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(false);

    // Handling the name change
    const handleName = (e) => {
        setName(e.target.value);
        setSubmitted(false);
    };

    // Handling the password change
    const handlePassword = (e) => {
        setPassword(e.target.value);
        setSubmitted(false);
    };

    // Handling the form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        if (name === '' || password === '') {
            setError(true);
        } else {
            setSubmitted(true);
            setError(false);
            // Placeholder values for NOT-NULL purposes
            const role = 2
            testRegister({name, password, role})
            // Hash the password and save to database
        }
    };

    // Showing error message if error is true
    const errorMessage = () => {
        return (
            <div
                className="error"
                style={{
                    display: error ? '' : 'none',
                }}>
                <h1>Please enter all the fields</h1>
            </div>
        );
    };

    // Showing success message
    const successMessage = () => {
        return (
            <div
                className="success"
                style={{
                    display: submitted ? '' : 'none',
                }}>
                <h1>User {name} successfully registered!!</h1>
            </div>
        );
    };

    return (
        <Layout>
            <div>
                <h1>User Registration</h1>
            </div>

            {/* Calling to the methods */}
            <div className="messages">
                {errorMessage()}
                {successMessage()}
            </div>

            <div className="flex-col border-2">
                <form>
                    {/* Labels and inputs for form data */}
                    <label className="label flex">Name</label>
                    <input onChange={handleName} className="input"
                           value={name} type="text" />

                    <label className="label flex">Password</label>
                    <input onChange={handlePassword} className="input"
                           value={password} type="password" />

                    <button onClick={handleSubmit} className="btn flex" type="submit">
                        Submit
                    </button>
                </form>
            </div>
        </Layout>
    );
}