import Layout from '../components/Layout'
import DashboardPanel from "../components/DashboardPanel";

export default function IndexPage() {
        return (
            <Layout title="Home | Next.js + TypeScript Example">
                <DashboardPanel/>
            </Layout>
        )
}
