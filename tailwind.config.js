/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'sans-serif': ['Roboto'],
      },
      colors: {
        'background-blue': '#230089',
        'accent-purple': '#5725E7',
      },
      boxShadow: {
        'custom': '1px 4px 10px rgba(149, 149, 149, 0.25)'
      },
      minHeight: {
        'layout': '105%'
      },
      maxHeight: {
        'layout': '105%',
      },
      maxWidth: {
        'layout': '100%',
      },
    },
  },
  plugins: [],
}
