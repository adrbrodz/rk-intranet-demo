export {}
const Pool = require('pg').Pool
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: 'admin',
    port: 5432,
});

const getUsers = () => {
    return new Promise(function(resolve, reject) {
        pool.query('SELECT * FROM users ORDER BY id ASC', (error, results) => {
            if (error) {
                reject(error)
            }
            resolve(results.rows);
        })
    })
}

const bcrypt = require('bcrypt');
const createUser = (body) => {
    return new Promise(function (resolve, reject) {
        const {name, password, role} = body
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(password, salt, function(err, hash) {
                (async () => {
                       await pool.query(
                         'INSERT INTO users (username, password_hash, password_salt, role_id) VALUES ($1, $2, $3, $4)',
                          [name, hash, salt, role]) //sql injection !!
                })().catch(err =>
                  setImmediate(() => {
                       throw err
                  }))
            });
        });
    })
}

const findUser = (body) => {
    return new Promise(function(resolve, reject) {
        const {name} = body
        pool.query('SELECT * FROM users WHERE username = $1', [name], (error, results) => {
            if (error) {
                reject(error)
            }
            resolve(results.rows)
        })
    })
}

const authenticateUser = (body) => {
    return new Promise(function(resolve, reject) {
        const password = body.password
        const hash = body.passwordHash
        let comp = bcrypt.compareSync(password, hash)
        resolve(comp)
    })
}

module.exports = {
    getUsers,
    createUser,
    findUser,
    authenticateUser
}