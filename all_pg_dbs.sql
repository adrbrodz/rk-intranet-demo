--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: access_rights_definitions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.access_rights_definitions (
    id integer NOT NULL,
    name character varying(300)
);


ALTER TABLE public.access_rights_definitions OWNER TO postgres;

--
-- Name: access_rights_mapping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.access_rights_mapping (
    user_id integer NOT NULL,
    access_rights_definitions_id integer
);


ALTER TABLE public.access_rights_mapping OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    role character varying(20) NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: tutorial_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_groups (
    name character varying(30) NOT NULL,
    tutorial_ids integer
);


ALTER TABLE public.tutorial_groups OWNER TO postgres;

--
-- Name: tutorials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorials (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    video_url character varying(150),
    context_text character varying(300)
);


ALTER TABLE public.tutorials OWNER TO postgres;

--
-- Name: user_tutorial_completion_submissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_tutorial_completion_submissions (
    user_id integer NOT NULL,
    tutorial_id integer NOT NULL,
    "timestamp" timestamp(4) without time zone NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.user_tutorial_completion_submissions OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(10) NOT NULL,
    password_hash character varying(100) NOT NULL,
    password_salt character varying(50) NOT NULL,
    date_last_login date,
    role_id integer NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: access_rights_definitions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.access_rights_definitions (id, name) FROM stdin;
\.


--
-- Data for Name: access_rights_mapping; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.access_rights_mapping (user_id, access_rights_definitions_id) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, role) FROM stdin;
0	tech_admin
1	moderator
2	user
\.


--
-- Data for Name: tutorial_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tutorial_groups (name, tutorial_ids) FROM stdin;
\.


--
-- Data for Name: tutorials; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tutorials (id, name, video_url, context_text) FROM stdin;
\.


--
-- Data for Name: user_tutorial_completion_submissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_tutorial_completion_submissions (user_id, tutorial_id, "timestamp", success) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, username, password_hash, password_salt, date_last_login, role_id) FROM stdin;
0	adrian	test	test	\N	2
1	adrbrodz	$2b$10$ngoRs2dKl9lYHCJ4a421o.aZ.wnYY9vLKew1w/YYDGul4DPoM//Du	x	\N	2
2	test	x	x	\N	2
4	asd	$2b$10$yoYZVx8WaERGyU.nbqjAV.3MyhhKL1jOmiTegNwueFfjuH2zqxP.y	$2b$10$yoYZVx8WaERGyU.nbqjAV.	\N	2
5	asdasdsad	$2b$10$stM5FH7HUz03hkx5AI0JJOA4jU66pOTCUQVHyKApySX/QC4LgToPm	$2b$10$stM5FH7HUz03hkx5AI0JJO	\N	2
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 5, true);


--
-- Name: access_rights_definitions access_rights_definitions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_rights_definitions
    ADD CONSTRAINT access_rights_definitions_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: tutorial_groups tutorial_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_groups
    ADD CONSTRAINT tutorial_groups_pkey PRIMARY KEY (name);


--
-- Name: tutorials tutorial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorials
    ADD CONSTRAINT tutorial_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: access_rights_mapping access_rights_mapping_access_rights_definitions_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_rights_mapping
    ADD CONSTRAINT access_rights_mapping_access_rights_definitions_id_fkey FOREIGN KEY (access_rights_definitions_id) REFERENCES public.access_rights_definitions(id);


--
-- Name: access_rights_mapping access_rights_mapping_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_rights_mapping
    ADD CONSTRAINT access_rights_mapping_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: user_tutorial_completion_submissions user_tutorial_completion_submissions_tutorial_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_tutorial_completion_submissions
    ADD CONSTRAINT user_tutorial_completion_submissions_tutorial_id_fkey FOREIGN KEY (tutorial_id) REFERENCES public.tutorials(id);


--
-- Name: user_tutorial_completion_submissions user_tutorial_completion_submissions_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_tutorial_completion_submissions
    ADD CONSTRAINT user_tutorial_completion_submissions_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users users_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- PostgreSQL database dump complete
--

