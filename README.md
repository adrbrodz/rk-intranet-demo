# TypeScript Next.js framework

## Installation
Install node dependencies
```
cd next-tailwind-typescript-starter
npm install
```
## Running the app
Starting the app locally
```
cd next-tailwind-typescript-starter
npm run dev
```
Restoring the database
```
psql -U postgres postgres < all_pg_dbs.sql
```
Running the server
```
cd node-postgres
node index.js
```
(Optional) Running Tailwind
```
cd styles
npx tailwindcss -i index.css -o ./dist/output.css --watch
```

