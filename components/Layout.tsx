import React, {ReactNode, useState} from 'react'
import Link from 'next/link'
import Head from 'next/head'

type Props = {
  children?: ReactNode
  title?: string
}

const Layout = ({ children, title = 'This is the default title', ...userLoggedIn}: Props ) => (
  <div className="flex flex-col p-20 h-screen bg-background-blue min-w-max">
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
      <div className="m-auto bg-white rounded-3xl p-20 min-h-layout min-w-full">
        <header className="flex">
            <div className="mb-10 text-4xl font-extrabold">
                <Link href="/">
                    <a>
                        <p>rk-<span className="text-red-500">intranet</span> Demo</p>
                    </a>
                </Link>
            </div>
          {/*<nav className="flex justify-center space-x-10">*/}
          {/*    <div>*/}
          {/*        <Link href="/">*/}
          {/*            <a>Home</a>*/}
          {/*        </Link>*/}
          {/*    </div>*/}
          {/*    <div>*/}
          {/*        <Link href="/about">*/}
          {/*            <a>About</a>*/}
          {/*        </Link>*/}
          {/*    </div>*/}
          {/*    <div>*/}
          {/*        <Link href="/users">*/}
          {/*            <a>Users List</a>*/}
          {/*        </Link>*/}
          {/*    </div>*/}
          {/*    <div>*/}
          {/*        <a href="/api/users">Users API</a>*/}
          {/*    </div>*/}
          {/*    <div>*/}
          {/*        <Link href="/machine-notes">*/}
          {/*            <a>Machine Notes</a>*/}
          {/*        </Link>*/}
          {/*    </div>*/}
          {/*</nav>*/}
        </header>
          <hr/>
      <div className="p-10 m-auto max-w-6xl">
          {children}
      </div>
      </div>
  </div>
)

export default Layout
