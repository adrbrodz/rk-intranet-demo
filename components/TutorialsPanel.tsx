import Link from "next/link";

export default function TutorialsPanel({}) {
    return (

        <div id="tutorials-panel" className="max-w-layout border rounded-xl shadow-custom p-5 grid grid-cols-3 justify-center">
            <div className="p-5 pb-3">
                <p className="text-sm mb-5 font-bold">Software tutorials</p>
                <hr/>
                <div className="mt-5 border rounded-sm shadow-custom text-xs">
                    <ul className="flex-col">
                        <div className="flex">
                                <li className="p-2 text-gray-300 grow">1.1 Software tutorial (1)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.2 Software tutorial (2)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.3 Software tutorial (3)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.4 Software tutorial (4)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.5 Software tutorial (5)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div>
                    </ul>
                </div>
            </div>
            <div className="p-5 pb-3">
                <p className="text-sm mb-5 font-bold">Custom regulations tutorials</p>
                <hr/>
                <div className="mt-5 border shadow-custom rounded-sm text-xs">
                    <ul className="flex-col">
                        <div className="flex">
                            <Link href="/tutorial">
                                <a className="font-bold grow p-2"><li>1.1 Custom regulations tutorial (1)</li></a>
                            </Link>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.2 Custom regulations tutorial (2)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.3 Custom regulations tutorial (3)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.4 Custom regulations tutorial (4)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.5 Custom regulations tutorial (5)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div>
                    </ul>
                </div>
            </div>
            <div className="p-5 pb-3">
                <p className="text-sm mb-5 font-bold">Workplace tutorials</p>
                <hr/>
                <div className="mt-5 border shadow-custom rounded-sm text-xs">
                    <ul className="flex-col">
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.1 Workplace tutorial (1)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.2 Workplace tutorial (2)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.3 Workplace tutorial (3)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.4 Workplace tutorial (4)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.5 Workplace tutorial (5)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div>
                    </ul>
                </div>
            </div>
            <div className="p-5">
                <p className="text-sm mb-5 font-bold">EHS tutorials</p>
                <hr/>
                <div className="mt-5 border shadow-custom rounded-sm text-xs">
                    <ul className="flex-col">
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.1 EHS tutorial (1)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.2 EHS tutorial (2)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.3 EHS tutorial (3)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.4 EHS tutorial (4)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.5 EHS tutorial (5)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div>
                    </ul>
                </div>
            </div>
            <div className="p-5">
                <p className="text-sm mb-5 font-bold">Other assignments</p>
                <hr/>
                <div className="mt-5 border shadow-custom rounded-sm text-xs">
                    <ul className="flex-col">
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.1 Other assignment (1)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.2 Other assignment (2)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.3 Other assignment (3)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.4 Other assignment (4)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div><hr/>
                        <div className="flex">
                            <li className="p-2 text-gray-300 grow">1.5 Other assignment (5)</li>
                            <input type="checkbox" className="my-auto mx-2"/>
                        </div>
                    </ul>
                </div>
            </div>
            <div className="p-5 mx-auto my-auto">
            </div>

        </div>
    )
}