import React, {useState} from "react";
import {useCookies} from "react-cookie";

export default function LoginForm({setUserLoggedIn}) {

    const [cookie, setCookie] = useCookies(["user"])
    const handleSignIn = async () => {
        try {
            const response = await findUser(name, password) //handle API call to sign in here.
            const data = response[0]
            setCookie("user", 'token', {
                path: "/",
                maxAge: 3600, // Expires after 1hr
                sameSite: true,
            })
        } catch (err) {
            console.log(err)
        }
    }

    async function findUser(name, password) {
        const userData = await fetch('http://localhost:3001/test', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({name})
        })
            .then(data => data.json())
            .then(data => {
                if (data != undefined) {
                    return (data)
                }
            })
        const passwordHash = userData[0].password_hash
        const credentials = ({password, passwordHash})
        const authSuccess = await fetch('http://localhost:3001/auth', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        })
            .then(data => data.json())
            .then(data => {
                return (data)
            })
        if ( authSuccess === true ) {
            setNoMatch(false);
            return userData
        }
        else {
            setNoMatch(true);
        }
    }

    // States for log-in management
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');

    // States for checking the errors
    const [noMatch, setNoMatch] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(false);

    // Handling the name change
    const handleName = (e) => {
        setName(e.target.value);
    };

    // Handling the password change
    const handlePassword = (e) => {
        setPassword(e.target.value);
    };

    // Handling the form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        if (name === '' || password === '') {
            setError(true);
        } else {
            if (name === 'user' && password === 'testing') {
                setUserLoggedIn(true)
            } else {
                setNoMatch(true)
            }
        }
    };

    // Showing error message if error is true
    const errorMessage = () => {
        return (
            <div>
                <div
                    className="error"
                    style={{
                        display: error ? '' : 'none',
                    }}>
                    <h1>Please enter all the fields</h1>
                </div>
                <div
                    className="no-match"
                    style={{
                        display: (noMatch === true && error === false) ? '' : 'none',
                    }}>
                    <h1>Incorrect email or password</h1>
                </div>
            </div>

        );
    };
    return (
    <div className="">
        <div className="my-10 mb-20 mx-auto bg-white shadow-md border border-gray-200 rounded-lg min-w-max max-w-sm p-4
                sm:p-6 lg:p-8 dark:bg-gray-800 dark:border-gray-700">
            <form className="space-y-6" action="#">
                <h3 className="text-xl font-bold text-gray-900 dark:text-white">Sign in to our
                    platform</h3>
                <div>
                    <div className="messages text-red-500 pb-3">
                        {errorMessage()}
                    </div>
                    <label htmlFor="email"
                           className="text-sm font-bold text-gray-900 block mb-2 dark:text-gray-300">Your
                        email</label>
                    <input onChange={handleName} type="email" name="email" id="email"
                           className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg
                                   focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600
                                   dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                           placeholder="name@company.com"/>
                </div>
                <div>
                    <label htmlFor="password"
                           className="text-sm font-bold text-gray-900 block mb-2 dark:text-gray-300">
                        Your password
                    </label>
                    <input onChange={handlePassword} type="password" name="password" id="password"
                           placeholder="••••••••"
                           className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg
                                   focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600
                                   dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"/>
                </div>
                <div className="flex items-start">
                    <div className="flex items-start">
                    </div>
                    <a href="#"
                       className="text-sm text-accent-purple hover:underline ml-auto dark:text-blue-500">
                        Lost Password?</a>
                </div>
                <button onClick={handleSubmit} type="submit"
                        className="w-full text-white bg-accent-purple hover:bg-accent-purple-800 focus:ring-4
                                focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center
                                dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                    Login to your account
                </button>
            </form>
        </div>
    </div>
    )
}