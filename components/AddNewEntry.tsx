const AddNewEntry = () => (
    <div className="p-20">
        <div className="content-box h-auto w-auto p-4 border-2">
            <div className="flex-col p-3">
                <label htmlFor="price" className="block text-sm font-medium text-gray-700">New Entry:</label>
                <div className="flex space-x-5">
                    <div className="grow mt-1 relative rounded-md shadow-md">
                        <input type="text" name="new-entry" id="new-entry" placeholder="Text..."
                               className="focus:ring-indigo-500 focus:border-indigo-500 block h-full w-full pl-7 pr-12
                               sm:text-sm border-gray-300 rounded-md placeholder:italic"
                        />
                    </div>
                    <button className="grow-0 rounded-full bg-slate-100 p-4">
                        <p className="text-xs mx-2">Add Note</p>
                    </button>
                </div>
            </div>
        </div>
    </div>

)

export default AddNewEntry
