import {useState} from "react";
import Link from "next/link";

const answers = {
    1: 'A',
    2: 'B',
    3: 'A',
}

const questions = {
    1: 'What does the acronym EEA stand for',
    '1A': 'European Economic Area',
    '1B': 'Efficient European Area',
    '1C': 'Economical European Area',
    '1D': 'European Efficient Area',
    2: 'Which country\'s regulations was the article dealing with?',
    '2A': 'Germany',
    '2B': 'Poland',
    '2C': 'UK',
    '2D': 'USA',
    3: 'How many member states of the European Union are members of Customs Union?',
    '3A': '27',
    '3B': 'None',
    '3C': 'All of them',
    '3D': 'Only Poland',
}

const TutorialTest = () => {
    const [questionNumber, setQuestionNumber] = useState(1);
    const [testScore, setTestScore] = useState(0);
    const [error, setError] = useState(undefined);
    const [results, setResults] = useState(false);

    function nextQuestion() {
        const currentScore = testScore
        const submittedAnswer = checkSubmittedAnswer()
        if (submittedAnswer) {
            setError(undefined)
            if (answers[questionNumber] == submittedAnswer) {
                setTestScore(currentScore + 1)
            }
            const currentNumber = questionNumber
            setQuestionNumber(currentNumber + 1)
        } else {
            setError('Please select an answer.')
        }

    }
    function checkSubmittedAnswer() {
        var ele = document.getElementsByName('default-radio') as HTMLInputElement | any;

        for (let i = 0; i < ele.length; i++) {
            if (ele[i].checked)
                return ele[i].value
        }
    }
    function showResults() {
        nextQuestion()
        setResults(true)
    }

    return (
        <div className="border rounded-xl shadow-custom p-5">
            <div className="p-5 max-w-full text-2xl">
                <Link href="/tutorial">
                    <a><p className="font-bold">2.1 Customs regulations tutorial (1)</p></a>
                </Link>
                <br/>
                <p className="mb-10">Includes customs regulations and contact information for this country's customs
                    office.</p>
                <hr/>
            </div>
            {
                results
                    ?
                    <div className="p-5 text-2xl">
                        <div className="space-y-10 text-center mt-20 mb-32">
                        <p className="font-bold ">Your results:</p>
                        <p className="font-bold ">{testScore}/3</p>
                        {
                            testScore < 3
                                ? <p><br/>You have failed to pass the test.<br/>Please try again.</p>
                                : <p className="text-green-500">Congratulations! You have succesfully passed the test.</p>
                        }
                        </div>
                    </div>
                    :
                    <div className="mx-10 my-5 text-sm grid grid-cols-2">
                        <div className="space-y-8">
                            <p className="font-bold">{questionNumber}/3</p>
                            <p className="font-bold">Q{questionNumber}. {questions[questionNumber]}</p>
                            <div className="border rounded-xl shadow-custom p-10 max-w-md">
                                <p className="text-red-500 font-bold">{error}</p>
                                <form>
                                    <ul className="p-5 space-y-3">
                                        <li>
                                            <input
                                                id = "default-radio-1"
                                                type = "radio"
                                                value = "A"
                                                name = "default-radio"
                                                className = "mr-5 w-4 h-4"/>
                                            <label>{questions[questionNumber+'A']}</label>
                                        </li>
                                        <li>
                                            <input
                                                id = "default-radio-2"
                                                type = "radio"
                                                value = "B"
                                                name = "default-radio"
                                                className = "mr-5 w-4 h-4"/>
                                            <label>{questions[questionNumber+'B']}</label>
                                        </li>
                                        <li>
                                            <input
                                                id = "default-radio-3"
                                                type = "radio"
                                                value = "C"
                                                name = "default-radio"
                                                className = "mr-5 w-4 h-4"/>
                                            <label>{questions[questionNumber+'C']}</label>
                                        </li>
                                        <li>
                                            <input
                                                id = "default-radio-4"
                                                type = "radio"
                                                value = "D"
                                                name = "default-radio"
                                                className = "mr-5 w-4 h-4"/>
                                            <label>{questions[questionNumber+'D']}</label>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <div className="ml-72 mt-80 p-5 mb-10">
                            {questionNumber < 3
                                ? <button onClick={nextQuestion}><p className="font-bold">Next question →</p></button>
                                : <button onClick={showResults}><p className="font-bold">Show results →</p></button> }
                        </div>
                    </div>
            }
        </div>
    )
}

export default TutorialTest
