import Link from "next/link";

const TutorialTemplate = () => {

    return (
        <div className="border rounded-xl shadow-custom p-5">
            <div className="p-5 max-w-full text-2xl">
                <p className="font-bold">2.1 Customs regulations tutorial (1)</p><br/>
                <p className="mb-10">Includes customs regulations and contact information for this country's customs
                    office.</p>
                <hr/>
            </div>
            <div className="grid grid-cols-2 max-h-max p-5 text-sm gap-24">
                <span className="overflow-auto max-h-96 overflow-auto">
                    <p className="text-accent-purple">Article</p><br/>
                    <p>Poland, as one of the 27 member states of the European Union, is a member of the Customs Union.</p><br/>
                    <p>The basic rules of the EU Customs Union include: no customs duties at internal borders between the
                        EU Member States; common customs duties on imports from outside the EU; common rules for origin of
                        products from outside the EU; and common definition of a customs value.</p><br/>

                    <p>Poland has adopted the Common Customs Tariff (CCT) of the EU which applies to goods imported from
                        outside Europe, while transactions carried out between Poland and the European Economic Area (EEA)
                        countries are free of duty.</p><br/>

                    <p>In general, EU external import duties are relatively low, especially for basic and raw goods
                    (around 5% on average). Applicable customs duty for a specific product imported from a selected
                    country of origin, can be found on the TARIC website. The combined Nomenclature of the European
                    Community (EC) integrates the Harmonized System (HS)nomenclature and has supplementary eight figure
                    subdivisions and its own legal notes created for community purposes.</p><br/>

                    <p>For goods from outside Europe, customs duties are calculated ad valorem on the Cost, Insurance
                        and Freight (CIF) value, in accordance with the Common Customs Tariff (CCT) for all the
                        countries of the Union. Customs authorities are primarily responsible for the supervision of the
                        EC’s international trade.</p><br/>

                    <p>(...)</p>
            </span>
                <div className="">
                    <p className="text-accent-purple">Video</p><br/>
                    <iframe className="rounded shadow-custom border-2 border-accent-purple" width="443" height="300" src="https://www.youtube.com/embed/7soJHCPF0KY"
                            title="YouTube video player" frameBorder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen></iframe>
                    <Link href="/test">
                        <a><p className="text-right font-bold mx-1 my-5 underline text-sm py-2">Take the test →</p></a>
                    </Link>

                </div>
            </div>
        </div>

    )
}
export default TutorialTemplate;